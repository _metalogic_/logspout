package main

import (
	_ "bitbucket.org/_metalogic_/logspout/healthcheck"
	_ "bitbucket.org/_metalogic_/logspout/adapters/raw"
	_ "bitbucket.org/_metalogic_/logspout/adapters/redis"
	_ "bitbucket.org/_metalogic_/logspout/adapters/syslog"
	_ "bitbucket.org/_metalogic_/logspout/adapters/multiline"
	_ "bitbucket.org/_metalogic_/logspout/httpstream"
	_ "bitbucket.org/_metalogic_/logspout/routesapi"
	_ "bitbucket.org/_metalogic_/logspout/transports/tcp"
	_ "bitbucket.org/_metalogic_/logspout/transports/udp"
	_ "bitbucket.org/_metalogic_/logspout/transports/tls"
)
