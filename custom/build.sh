#!/bin/sh
set -e
apk add --update go build-base git ca-certificates
mkdir -p /go/src/bitbucket.org/_metalogic_
cp -r /src /go/src/bitbucket.org/_metalogic_/logspout
cd /go/src/bitbucket.org/_metalogic_/logspout
export GOPATH=/go
go get
go build -ldflags "-X main.Version=$1" -o /bin/logspout
apk del go git build-base
rm -rf /go /var/cache/apk/*

# backwards compatibility
ln -fs /tmp/docker.sock /var/run/docker.sock
