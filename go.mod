module bitbucket.org/_metalogic_/logspout

go 1.15

require (
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/Sirupsen/logrus v0.0.0-20160601113210-f3cfb454f4c2 // indirect
	github.com/docker/docker v0.0.0-20160708193732-ad969f1aa782 // indirect
	github.com/docker/engine-api v0.0.0-20160708123604-98348ad6f9c8 // indirect
	github.com/docker/go-units v0.3.1 // indirect
	github.com/fsouza/go-dockerclient v0.0.0-20160624230725-1a3d0cfd7814
	github.com/garyburd/redigo v1.6.0
	github.com/gorilla/context v0.0.0-20160525203319-aed02d124ae4 // indirect
	github.com/gorilla/mux v0.0.0-20160605233521-9fa818a44c2b
	github.com/hashicorp/go-cleanhttp v0.0.0-20160407174126-ad28ea4487f0 // indirect
	github.com/jmoiron/jsonq v0.0.0-20150511023944-e874b168d07e
	github.com/opencontainers/runc v0.0.0-20160706165155-9d7831e41d3e // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20160707223729-f841c39de738
)
