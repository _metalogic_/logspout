package redis

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/logspout/router"
	"github.com/garyburd/redigo/redis"
)

const (
	NO_MESSAGE_PROVIDED     = "no message"
	LOGTYPE_APPLICATIONLOG  = "applog"
	LOGTYPE_ACCESSLOG       = "accesslog"
	DEFAULT_CONNECT_TIMEOUT = 100
	DEFAULT_READ_TIMEOUT    = 300
	DEFAULT_WRITE_TIMEOUT   = 500
)

// RedisAdapter ...
type RedisAdapter struct {
	route        *router.Route
	pool         *redis.Pool
	key          string
	dockerHost   string
	logstashType string
	dedotLabels  bool
	muteErrors   bool
	msgCounter   int
}

// DockerFields ...
type DockerFields struct {
	Name       string            `json:"name"`
	CID        string            `json:"cid"`
	Image      string            `json:"image"`
	ImageTag   string            `json:"image_tag,omitempty"`
	Source     string            `json:"source"`
	DockerHost string            `json:"docker_host,omitempty"`
	Labels     map[string]string `json:"labels,omitempty"`
}

// LogstashFields ...
type LogstashFields struct {
	Docker DockerFields `json:"docker"`
}

// LogstashMessage ...
type LogstashMessage struct {
	Type       string       `json:"@type,omitempty"`
	Timestamp  string       `json:"@timestamp"`
	Sourcehost string       `json:"host"`
	Message    string       `json:"message"`
	Fields     DockerFields `json:"docker"`
	Logtype    string       `json:"logtype,omitempty"`
	// Only one of the following 3 is initialized and used, depending on the incoming json:logtype
	LogtypeAccessfields map[string]interface{} `json:"accesslog,omitempty"`
	LogtypeAppfields    map[string]interface{} `json:"applog,omitempty"`
	LogtypeEventfields  map[string]interface{} `json:"event,omitempty"`
}

func init() {
	router.AdapterFactories.Register(NewRedisAdapter, "redis")
}

// NewRedisAdapter ...
func NewRedisAdapter(route *router.Route) (router.LogAdapter, error) {
	// add port if missing
	address := route.Address
	if !strings.Contains(address, ":") {
		address = address + ":6379"
	}

	// get our config keys, first from the route options (e.g. redis://<host>?opt1=val&opt1=val&...)
	// if route option is missing, attempt to get the value from the environment
	key := getopt(route.Options, "key", "REDIS_KEY", "logspout")
	password := getopt(route.Options, "password", "REDIS_PASSWORD", "")
	dockerHost := getopt(route.Options, "docker_host", "REDIS_DOCKER_HOST", "")
	logstashType := getopt(route.Options, "logstash_type", "REDIS_LOGSTASH_TYPE", "")
	dedotLabels := getopt(route.Options, "dedot_labels", "DEDOT_LABELS", "false") == "true"
	debug := getopt(route.Options, "debug", "DEBUG", "") != ""
	muteErrors := getopt(route.Options, "mute_errors", "MUTE_ERRORS", "true") == "true"

	connectTimeout := getintopt(route.Options, "connect_timeout", "CONNECT_TIMEOUT", DEFAULT_CONNECT_TIMEOUT)
	readTimeout := getintopt(route.Options, "read_timeout", "READ_TIMEOUT", DEFAULT_READ_TIMEOUT)
	writeTimeout := getintopt(route.Options, "write_timeout", "WRITE_TIMEOUT", DEFAULT_WRITE_TIMEOUT)

	databaseOpt := getopt(route.Options, "database", "REDIS_DATABASE", "0")
	database, err := strconv.Atoi(databaseOpt)
	if err != nil {
		return nil, errorf("Invalid Redis database number specified: %s. Please verify & fix", databaseOpt)
	}

	if debug {
		log.SetLevel(log.DebugLevel)
	}

	log.Debugf("Using Redis server '%s', dbnum: %d, password?: %t, pushkey: '%s', logstash type: '%s'\n",
		address, database, password != "", key, logstashType)
	log.Debugf("Dedotting docker labels: %t", dedotLabels)
	log.Infof("Timeouts set, connect: %dms, read: %dms, write: %dms\n", connectTimeout, readTimeout, writeTimeout)
	if connectTimeout+readTimeout+writeTimeout > 950 {
		log.Infof("WARN: sum of connect, read & write timeouts > 950 ms. You risk loosing container logs as Logspout stops pumping logs after a 1.0 second timeout.")
	}

	pool := newRedisConnectionPool(address, password, database, connectTimeout, readTimeout, writeTimeout)

	// lets test the water
	conn := pool.Get()
	defer conn.Close()
	res, err := conn.Do("PING")
	if err != nil {
		return nil, errorf("Cannot connect to Redis server %s: %v", address, err)
	}

	log.Debugf("Redis connect successful, got response: %s\n", res)

	return &RedisAdapter{
		route:        route,
		pool:         pool,
		key:          key,
		dockerHost:   dockerHost,
		logstashType: logstashType,
		dedotLabels:  dedotLabels,
		muteErrors:   muteErrors,
		msgCounter:   0,
	}, nil
}

// Stream ...
func (a *RedisAdapter) Stream(logstream chan *router.Message) {
	conn := a.pool.Get()
	defer conn.Close()

	mute := false

	for m := range logstream {
		a.msgCounter++
		msgID := fmt.Sprintf("%s#%d", m.Container.ID[0:12], a.msgCounter)

		js, err := createLogstashMessage(m, a.dockerHost, a.logstashType, a.dedotLabels)
		if err != nil {
			if a.muteErrors {
				if !mute {
					log.Infof("redis[%s]: error on json.Marshal (muting until recovered): %s\n", msgID, err)
					mute = true
				}
			} else {
				log.Infof("redis[%s]: error on json.Marshal: %s\n", msgID, err)
			}
			continue
		}
		_, err = conn.Do("RPUSH", a.key, js)
		if err != nil {
			if a.muteErrors {
				if !mute {
					log.Infof("redis[%s]: error on rpush (muting until restored): %s\n", msgID, err)
				}
			} else {
				log.Infof("redis[%s]: error on rpush: %s\n", msgID, err)
			}
			mute = true

			// first close old connection
			conn.Close()

			// next open new connection
			conn = a.pool.Get()

			// since message is already marshaled, send again
			_, err = conn.Do("RPUSH", a.key, js)
			if err != nil {
				conn.Close()
				if !a.muteErrors {
					log.Infof("redis[%s]: error on rpush (retry): %s\n", msgID, err)
				}
			} else {
				log.Infof("redis[%s]: successful retry rpush after error\n", msgID)
				mute = false
			}

			continue
		} else {
			if mute {
				log.Infof("redis[%s]: successful rpush after error\n", msgID)
				mute = false
			}
		}
	}
}

func errorf(format string, a ...interface{}) (err error) {
	err = fmt.Errorf(format, a...)
	if os.Getenv("DEBUG") != "" {
		fmt.Println(err.Error())
	}
	return
}

func getopt(options map[string]string, optkey string, envkey string, defaultValue string) (value string) {
	value = options[optkey]
	if value == "" {
		value = os.Getenv(envkey)
		if value == "" {
			value = defaultValue
		}
	}
	return
}
func getintopt(options map[string]string, optkey string, envkey string, defaultValue int) (value int) {
	valueOpt := options[optkey]
	if valueOpt == "" {
		valueOpt = os.Getenv(envkey)
	}
	if valueOpt == "" {
		value = defaultValue
	} else {
		var err error
		value, err = strconv.Atoi(valueOpt)
		if err != nil {
			log.Errorf("Invalid value for integer paramater %s: %s - using default: %d\n", optkey, valueOpt, defaultValue)
			value = defaultValue
		}
	}
	return
}

func newRedisConnectionPool(server, password string, database int, connectTimeout int, readTimeout int, writeTimeout int) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     1,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server,
				redis.DialConnectTimeout(time.Duration(connectTimeout)*time.Millisecond),
				redis.DialReadTimeout(time.Duration(readTimeout)*time.Millisecond),
				redis.DialWriteTimeout(time.Duration(writeTimeout)*time.Millisecond))
			if err != nil {
				return nil, err
			}
			if password != "" {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					return nil, err
				}
			}
			if database > 0 {
				if _, err := c.Do("SELECT", database); err != nil {
					c.Close()
					return nil, err
				}
			}
			return c, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			if err != nil {
				log.Error("redis: test on borrow failed: ", err)
			}
			return err
		},
	}
}

func splitImage(imageTag string) (image string, tag string) {
	colon := strings.LastIndex(imageTag, ":")
	sep := strings.LastIndex(imageTag, "/")
	if colon > -1 && sep < colon {
		image = imageTag[0:colon]
		tag = imageTag[colon+1:]
	} else {
		image = imageTag
	}
	return
}

func dedotLabels(labels map[string]string) map[string]string {
	for key := range labels {
		if strings.Contains(key, ".") {
			dedottedLabel := strings.Replace(key, ".", "_", -1)
			labels[dedottedLabel] = labels[key]
			delete(labels, key)
		}
	}

	return labels
}

func createLogstashMessage(m *router.Message, dockerHost string, logstashType string, dedot bool) ([]byte, error) {
	image, imageTag := splitImage(m.Container.Config.Image)
	cid := m.Container.ID[0:12]
	name := m.Container.Name[1:]
	timestamp := m.Time.UTC().Format(time.RFC3339Nano)

	msg := LogstashMessage{}

	msg.Type = logstashType
	msg.Timestamp = timestamp
	msg.Sourcehost = m.Container.Config.Hostname
	msg.Fields.CID = cid
	msg.Fields.Name = name
	msg.Fields.Image = image
	msg.Fields.ImageTag = imageTag
	msg.Fields.Source = m.Source
	msg.Fields.DockerHost = dockerHost

	// see https://github.com/rtoma/logspout-redis-logstash/issues/11
	if dedot {
		msg.Fields.Labels = dedotLabels(m.Container.Config.Labels)
	} else {
		msg.Fields.Labels = m.Container.Config.Labels
	}

	// Check if the message to log itself is json
	if validJSONMessage(strings.TrimSpace(m.Data)) {
		// So it is, include it in the Logstashmessage
		err := msg.UnmarshalDynamicJSON([]byte(m.Data))
		if err != nil {
			// Can't unmarshall the json (invalid?), put it in message
			msg.Message = m.Data
		} else if msg.Message == "" {
			msg.Message = NO_MESSAGE_PROVIDED
		}
	} else {
		// Regular logging (no json)
		msg.Message = m.Data
	}
	return json.Marshal(msg)

}

func validJSONMessage(s string) bool {

	if !strings.HasPrefix(s, "{") || !strings.HasSuffix(s, "}") {
		return false
	}
	return true
}

// UnmarshalDynamicJSON ...
func (d *LogstashMessage) UnmarshalDynamicJSON(data []byte) error {
	var dynMap map[string]interface{}

	if d == nil {
		return errors.New("RawString: UnmarshalJSON on nil pointer")
	}

	if err := json.Unmarshal(data, &dynMap); err != nil {
		return err
	}

	// Take logtype of the hash, but only if it is a valid logtype
	if _, ok := dynMap["logtype"].(string); ok {
		if dynMap["logtype"].(string) == LOGTYPE_APPLICATIONLOG || dynMap["logtype"].(string) == LOGTYPE_ACCESSLOG {
			d.Logtype = dynMap["logtype"].(string)
			delete(dynMap, "logtype")
		}
	}
	// Take message out of the hash
	if _, ok := dynMap["message"]; ok {
		d.Message = dynMap["message"].(string)
		delete(dynMap, "message")
	}

	// Only initialize the "used" hash in struct
	if d.Logtype == LOGTYPE_APPLICATIONLOG {
		d.LogtypeAppfields = make(map[string]interface{}, 0)
	} else if d.Logtype == LOGTYPE_ACCESSLOG {
		d.LogtypeAccessfields = make(map[string]interface{}, 0)
	} else {
		d.LogtypeEventfields = make(map[string]interface{}, 0)
	}

	// Fill the right hash based on logtype
	for key, val := range dynMap {
		if d.Logtype == LOGTYPE_APPLICATIONLOG {
			d.LogtypeAppfields[key] = val
		} else if d.Logtype == LOGTYPE_ACCESSLOG {
			d.LogtypeAccessfields[key] = val
		} else {
			d.LogtypeEventfields[key] = val
		}
	}

	return nil
}
