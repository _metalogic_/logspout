FROM golang:1.15 AS builder

WORKDIR /build

COPY ./ /build

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o cmd .

FROM metalogic/alpine:3.10.3

VOLUME /mnt/routes
EXPOSE 80

COPY --from=builder /build/cmd /bin/logspout

CMD ["/bin/logspout"]

HEALTHCHECK --interval=30s CMD /usr/local/bin/health http://localhost:80/health
